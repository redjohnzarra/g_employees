<?php
    require_once 'config.php';

    if(array_key_exists('m', $_GET)){
        $module = $_GET['m'];
    }else{
        $module = 'index';
    }

    switch ($module) {
        case 'employee':
            require_once 'controller/EmployeeController/Employee.php';
            $controllerObj = new Employee;
            break;

        case 'index':
            require_once 'controller/EmployeeController/Employee.php';
            $controllerObj = new Employee;
            break;

        default:
            require_once 'view/error/error.php';
            break;
    }

    if(isset($controllerObj)){
        if(!array_key_exists('act', $_GET)){
            $controllerObj->display();
        }else{
            switch($_GET['act']) {
                case 'add-employee':
                $controllerObj->addEmployee();
                break;
                case 'save-employee':
                $controllerObj->saveEmployee();
                break;
                case 'update-employee':
                $controllerObj->updateEmployee();
                break;
                case 'delete-employee':
                $controllerObj->deleteEmployee();
                break;
                case 'check-employee-exists':
                $controllerObj->checkEmployeeExists();
                break;

                default:
                # code...
                break;
            }
        }
    }
?>
