<?php require_once APP_ROOT.'view/header.php' ?>
    <div class="table-cont">
        <div class="row">
            <h3>List of Employees</h3><br>
        </div>
        <div class="row">
            <a class="pull-right" href="javascript:void(0)" onclick="addEmployee()">Add New Employee</a>
        </div><br>
        <div class="row">
            <table class="employees-table hover row-border order-column" cellspacing="0" width="100%">
                <?php
                    if(empty($employees)){
                        print("<thead>
                            <tr>
                                <th></th>
                            </tr></thead>
                            <tbody>
                                <tr>
                                    <td class='text-center'>No Results</td>
                                </tr>
                            </tbody>");
                    }else{
                        /*TODO*/
                        print('
                            <thead>
                                <tr class="bg-info">
                                    <th>Employee Number</th>
                                    <th>Last Name</th>
                                    <th>First Name</th>
                                    <th>Address</th>
                                    <th>Gender</th>
                                    <th>Date of Birth</th>
                                    <th>Age</th>
                                    <th>Email Address</th>
                                    <th>Status</th>
                                    <th>Spouse</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>');
                        foreach($employees as $empy){
                            print('
                            <tr>
                                <td>'.$empy['EmpyNo'].'</td>
                                <td class="n-wrap">'.ucfirst($empy['LName']).'</td>
                                <td class="n-wrap">'.ucfirst($empy['FName']).'</td>
                                <td>'.$empy['Address'].'</td>
                                <td>'.ucfirst($empy['Gender']).'</td>
                                <td>'.date("m/d/Y", strtotime($empy['DBirth'])).'</td>
                                <td>'.$empy['age'].'</td>
                                <td>'.$empy['Email'].'</td>
                                <td>'.ucfirst($empy['Status']).'</td>
                                <td class="n-wrap">'.ucfirst($empy['Spouse']).'</td>
                                <td><a href="javascript:void(0)" onclick="updateEmployee('.$empy['id'].')" title="Edit" data-toggle="tooltip"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;<a href="javascript:void(0)" onclick="confirmDelete('.$empy['id'].')" title="Delete" data-toggle="tooltip"><span class="glyphicon glyphicon-remove"></span></a></td>
                            </tr>');
                        }
                        echo '</tbody>';
                    }
                ?>
            </table>
        </div>
    </div>

<?php require_once APP_ROOT.'view/footer.php' ?>
