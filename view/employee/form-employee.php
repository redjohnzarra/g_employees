<div class="modal fade" id="<?= $act ?>-employee-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-<?php if($act == 'update') echo 'green'; else echo 'primary'; ?>">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"><?= ucfirst($act) ?> Employee</h4>
            </div>
            <div class="modal-body">
                <form id="<?= $act ?>-employee">
                    <div class="row">
                        <div class="col-md-6"><!-- class form-inline for inline -->
                            <div class="form-group">
                                <label for="employee_number">Employee Number</label>
                                <input type="text" class="form-control" name="e_number" id="e_number" <?php if($act == 'update') echo 'value="'.$empyRecord['EmpyNo'].'"'; ?>>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_birth">Date of Birth</label>
                                <input type="text" class="form-control datepicker-dob" name="e_date_of_birth" id="e_date_of_birth" <?php if($act == 'update') echo 'value="'.date("M d, Y", strtotime($empyRecord['DBirth'])).'"'; ?>>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" name="e_last_name" id="e_last_name" <?php if($act == 'update') echo 'value="'.$empyRecord['LName'].'"'; ?>>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" class="form-control" name="e_first_name" id="e_first_name" <?php if($act == 'update') echo 'value="'.$empyRecord['FName'].'"'; ?>>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="e_email" id="e_email" <?php if($act == 'update') echo 'value="'.$empyRecord['Email'].'"'; ?>>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group inp-radio">
                                <label class="inp-btn">Status</label>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5"><input type="radio" name="e_status" id="e_status_s" value="single" <?php if($act == 'update' && $empyRecord['Status'] == 'single') echo 'checked'; ?>> <label for="e_status_s">Single</label></div>
                                    <div class="col-md-5"><input type="radio" name="e_status" id="e_status_m" value="married" <?php if($act == 'update' && $empyRecord['Status'] == 'married') echo 'checked'; ?>> <label for="e_status_m">Married</label></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-5"><input type="radio" name="e_status" id="e_status_d" value="divorced" <?php if($act == 'update' && $empyRecord['Status'] == 'divorced') echo 'checked'; ?>> <label for="e_status_d">Divorced</label></div>
                                    <div class="col-md-5"><input type="radio" name="e_status" id="e_status_w" value="widowed" <?php if($act == 'update' && $empyRecord['Status'] == 'widowed') echo 'checked'; ?>> <label for="e_status_w">Widowed</label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" name="e_address" id="e_address" <?php if($act == 'update') echo 'value="'.$empyRecord['Address'].'"'; ?>>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="spouse">Spouse</label>
                                <input type="text" class="form-control" name="e_spouse" id="e_spouse" <?php if($act == 'update'){ echo 'value="'.$empyRecord['Spouse'].'"'; if($empyRecord['Status'] == 'single') echo 'disabled'; }?>>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group inp-radio">
                                <label class="inp-btn">Gender</label><br>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4"><input type="radio" name="e_gender" id="e_gender_m" value="male" <?php if($act == 'update' && $empyRecord['Gender'] == 'm') echo 'checked'; ?>> <label for="e_gender_m">Male</label></div>
                                    <div class="col-md-4"><input type="radio" name="e_gender" id="e_gender_f" value="female" <?php if($act == 'update' && $empyRecord['Gender'] == 'f') echo 'checked'; ?>> <label for="e_gender_f">Female</label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <?php if($act == 'add'){?>
                <button type="button" class="btn btn-primary" onclick="validateEmployeeForm('add')">Save</button>
            <?php }elseif($act == 'update'){?>
                <button type="button" class="btn btn-green" onclick="validateEmployeeForm('update', '<?= $id ?>')">Update</button>
            <?php }?>
            <!-- <button type="button" class="btn btn-primary" onclick="validateEmployeeForm('add')">Save</button> -->
            </div>
        </div>
    </div>
</div>
