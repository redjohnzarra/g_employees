<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employees</title>

    <link rel="stylesheet" href="includes/css/jquery-dataTables.min.css">
    <link rel="stylesheet" href="includes/css/jquery-ui.min.css">
    <link rel="stylesheet" href="includes/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/employees.css">
</head>
<body>
    <div class="container">
