<?php
    /**
     * Class for EmployeeController. Contains function/s for things related to employees like adding, updating, deleting, etc.
     */
    class Employee {
        public function __construct(){
            $this->conn = Database::getConnection();
        }

        /**
         * default function for Class employee. Automatically chosen when the act given on index.php is not on the list.
         * @return [type] [description]
         */
        public function display(){
            $sql = "SELECT *, TIMESTAMPDIFF(YEAR, DBirth, NOW()) AS age FROM employees";
            $results = mysqli_query($this->conn, $sql);
            $employees = array();
            while($row = mysqli_fetch_assoc($results)){
                $employees[] = $row;
            }

            require_once 'view/employee/employee.php';
        }

        /**
         * function that renders the view for the add employee form
         */
        public function addEmployee(){
            $act = 'add';
            require_once 'view/employee/form-employee.php';
        }

        /**
         * function that inserts/updates (depending on the act) an employee's record to the database
         * @return [type] [description]
         */
        public function saveEmployee(){
            if(array_key_exists("act", $_POST)){
                $act = $_POST['act'];
                $id = array_key_exists('id', $_POST) ? $_POST['id'] : '';
                $eNumber = $_POST['e_number'];
                $eLastName = $_POST['e_last_name'];
                $eFirstName = $_POST['e_first_name'];
                $eDateOfBirth = date("Y-m-d", strtotime($_POST['e_date_of_birth']));
                $eGender = $_POST['e_gender'] == 'male' ? 'm' : 'f';
                $eEmail = $_POST['e_email'];
                $eAddress = $_POST['e_address'];
                $eStatus = $_POST['e_status'];
                $eSpouse = $_POST['e_spouse'];

                if($act == 'add'){
                    $sql = "INSERT INTO employees (EmpyNo, LName, FName, Address, Gender, DBirth, Email, Status, Spouse) VALUES ('$eNumber', '$eLastName', '$eFirstName', '$eAddress', '$eGender', '$eDateOfBirth', '$eEmail', '$eStatus', '$eSpouse')";
                }elseif($act == 'update'){
                    $sql = "UPDATE employees SET EmpyNo = '$eNumber', LName = '$eLastName', FName = '$eFirstName', Address = '$eAddress', Gender = '$eGender', DBirth = '$eDateOfBirth', Email = '$eEmail', Status = '$eStatus', Spouse = '$eSpouse' WHERE id = '$id'";
                }

                if ($this->conn->query($sql) === TRUE) {
                    $title = "Success";
                    $msg = $act == 'add' ? 'New employee added successfully!': 'Employee record updated successfully!';
                }else{
                    $title = "Error";
                    $msg = "Error: " . $this->conn->error;
                }

                $this->returnResponse($title, $msg);
            }
        }

        /**
         * function that loads the page for editing employee's record
         * @return [type] [description]
         */
        public function updateEmployee(){
            if(array_key_exists("id", $_POST)){
                $act = 'update';
                $id = $_POST['id'];

                $sql = "SELECT * FROM employees WHERE id = '$id'";
                $results = mysqli_query($this->conn, $sql);
                $empyRecord = array();
                $empyRecord = mysqli_fetch_assoc($results);

                require_once 'view/employee/form-employee.php';
            }
        }

        /**
         * function that deletes an employees record from the database
         * @return [type] [description]
         */
        public function deleteEmployee(){
            if(array_key_exists("id", $_POST)){
                $id = $_POST['id'];

                $sql = "DELETE FROM employees WHERE id = '$id'";
                if ($this->conn->query($sql) === TRUE) {
                    $title = "Success";
                    $msg = 'Record deleted successfully!';
                }else{
                    $title = "Error";
                    $msg = "Error: " . $this->conn->error;
                }

                $this->returnResponse($title, $msg);
            }
        }

        /**
         * function that returns a response dialog to be displayed after a successful ajax call
         * @param  str $title title for the response dialog. eg. Error, Success
         * @param  str $msg   Message for the response dialog in relation to the title.
         * @return [type]        [description]
         */
        public function returnResponse($title, $msg){
            $titleClass = $title == 'Success' ? "bg-success" : "bg-danger";
            $glyphIcon = $title == 'Success' ? 'thumbs-up' : 'thumbs-down';
            $response = '<div class="modal fade" id="response-dialog" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document"><div class="modal-content">
                                <div class="modal-header '.$titleClass.'">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">'.$title.'&nbsp;&nbsp;<span class="glyphicon glyphicon-'.$glyphIcon.'"></span></h4>
                                </div>
                            <div class="modal-body">'.$msg.'</div>
                            <div class="modal-footer"></div></div></div></div>';
            echo $response;
        }

        /**
         * [checkEmployeeExists description]
         * @return [type] [description]
         */
        public function checkEmployeeExists(){
            $empyNo = $_POST['e_number'];
            $sql = "SELECT * FROM employees WHERE EmpyNo = '$empyNo'";
            $results = mysqli_query($this->conn, $sql);

            echo mysqli_num_rows($results);
        }
    }

?>
