<?php

    define('HOST_URL', 'http://'.$_SERVER['HTTP_HOST'].'/g_employees/'); //change g_employees to whatever your project name directory is
    define('APP_ROOT', realpath( dirname( __FILE__ ) ).'/');
    define('DB_HOST', 'localhost'); //change if your host isn't localhost
    define('DB_NAME', 'db_employees');
    define('DB_USER', 'username'); //change to whatever your preferred mysql user is
    define('DB_PASS', 'password'); //change to the password for the mysql user set above

    /**
     * Class for Database connection
     */
    class Database {

        private static $db;
        private $connection;

        public function __construct() {
         $this->connection = new MySQLi(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        }

        public function __destruct() {
         $this->connection->close();
        }

        /**
         * function that creates the database connection
         * @return dbconnection
         */
        public static function getConnection() {
            if (self::$db == null) {
                self::$db = new Database();
            }

            return self::$db->connection;
        }
   }
?>
