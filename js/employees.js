var BASE_URL = window.location.origin + window.location.pathname;

$(document).ready(function(){
    if($(".employees-table").size() > 0){
        $(".employees-table").dataTable();
    }

    if($('[data-toggle="tooltip"]').size() > 0){
        $('[data-toggle="tooltip"]').tooltip();
    }

    if($(".datepicker").size() > 0){
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true
        });
    }
});

function addEmployee(){
    $.ajax({
        type: "GET",
        url: BASE_URL + "?m=employee&act=add-employee",
        success: function(response){
            $("body").append(response);
            $(".datepicker-dob").datepicker({
                dateFormat: 'M dd, yy',
                maxDate: '-15yr',
                changeMonth: true,
                yearRange: '1920:',
                changeYear: true,
                defaultDate: '-25yr'
            });
            $("#add-employee-modal").on("hidden.bs.modal", function(){
                $('#add-employee-modal').remove(); //reset elements after loading
            });
            $('#add-employee-modal').modal({
                backdrop: 'static'
            });

            checkStatus();
        }
    });
}

function checkStatus(){
    $('input[type=radio][name=e_status]').on('change', function() {
        if($(this).val() == 'single'){
            $("input[name='e_spouse']").prop("disabled", true);
        }else{
            $("input[name='e_spouse']").prop("disabled", false);
        }
    });
}


function validateEmployeeForm(act, employeeId){
    // console.log($("input[name='e_date_of_birth']").val()); return;
    $("#"+act+"-employee").validate({
        rules: {
            e_number: {
                required: true
            },
            e_date_of_birth: {
                required: true
            },
            e_last_name: {
                required: true
            },
            e_email: {
                required: true
            },
            e_first_name: {
                required: true
            },
            e_status: {
                required: true
            },
            e_address: {
                required: true
            },
            e_gender: {
                required: true
            }

        },
        showErrors: function(errorMap, errorList) {
            // Clean up any tooltips for valid elements
            $.each(this.validElements(), function (index, element) {
              var $element = $(element);

                if($($element).attr("type") == 'radio'){
                    formGroupContainer = $element.parent().parent().parent();
                    formGroupContainer.data("title", "").removeClass('radio-error').tooltip("destroy");
                }else{
                    $element.data("title", "") // Clear the title - there is no error associated anymore
                      .removeClass("error")
                      .tooltip("destroy");
                }
            });
            // Create new tooltips for invalid elements
            $.each(errorList, function (index, error) {
              var $element = $(error.element);
                if($($element).attr("type") == 'radio'){
                    formGroupContainer = $element.parent().parent().parent();
                    formGroupContainer.tooltip("destroy").data("title", error.message).addClass('radio-error').tooltip();
                }else{
                    $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                      .data("title", error.message)
                      .addClass("error")
                      .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
                }
            });
        },
        //perform AJAX
        submitHandler: function() {
            if(act == 'add'){
                $.ajax({
                    url: BASE_URL + "?m=employee&act=check-employee-exists",
                    type: "POST",
                    data: {
                        e_number: $("input[name='e_number']").val()
                    },
                    success: function(response){
                        if(response > 0){
                            $("input[name='e_number']").data("title", "Employee number already exists").addClass('error').tooltip();
                        }else{
                            saveEmployee(act, employeeId);
                        }
                    }
                });
            }else{
                saveEmployee(act, employeeId);
            }
        }
    });

    if($("input[name='e_status']:checked").val() == 'single'){
        statusVal = false;
    }else{
        statusVal = true;
    }

    $("input[name='e_spouse']").rules('add', {
        required: statusVal
    });

    $("#"+act+"-employee").submit();
}

function saveEmployee(act, id){
    if($("input[name='e_spouse']").prop('disabled') == true){
        spouseName = '';
    }else{
        spouseName = $("input[name='e_spouse']").val();
    }
    $.ajax({
        type: "POST",
        url: BASE_URL + "?m=employee&act=save-employee",
        data: {
            act: act,
            id: id,
            e_number: $("input[name='e_number']").val(),
            e_last_name: $("input[name='e_last_name']").val(),
            e_first_name: $("input[name='e_first_name']").val(),
            e_date_of_birth: $("input[name='e_date_of_birth']").val(),
            e_gender: $("input[name='e_gender']:checked").val(),
            e_email: $("input[name='e_email']").val(),
            e_address: $("input[name='e_address']").val(),
            e_status: $("input[name='e_status']:checked").val(),
            e_spouse: spouseName
        },
        success: function(response){
            closeModal(act);
            $("body").append(response);
            initModal('response-dialog');
        }
    });
}

function updateEmployee(id){
    $.ajax({
        type: "POST",
        url: BASE_URL + "?m=employee&act=update-employee",
        data: {
            id: id
        },
        success: function(response){
            $("body").append(response);
            $(".datepicker-dob").datepicker({
                dateFormat: 'M dd, yy',
                maxDate: '-15yr',
                changeMonth: true,
                yearRange: '1920:',
                changeYear: true,
                defaultDate: '-25yr'
            });
            $("#update-employee-modal").on("hidden.bs.modal", function(){
                $('#update-employee-modal').remove(); //reset elements after loading
            });
            $('#update-employee-modal').modal({
                backdrop: 'static'
            });

            checkStatus();
        }
    });
}

function closeModal(act){
    $("div#"+act+"-employee-modal").modal("hide");
}

function confirmDelete(id){
    $("body").append('<div class="modal fade" id="delete-employee-modal" tabindex="-1" role="dialog"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header bg-danger"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">Confirm Delete</h4></div><div class="modal-body">Are you sure you want to delete this record?</div><div class="modal-footer"><button type="button" class="btn btn-danger" onclick="deleteEmployee('+id+')">Yes</button><button type="button" class="btn btn-default" data-dismiss="modal">No</button></div></div></div></div>');

    $("#delete-employee-modal").on("hidden.bs.modal", function(){
        $("#delete-employee-modal").remove();
    });

    $("#delete-employee-modal").modal("show");
}

function deleteEmployee(id){
    $.ajax({
        type: "POST",
        url: BASE_URL + "?m=employee&act=delete-employee",
        data: {
            id: id
        },
        success: function(response){
            closeModal('delete');
            $("body").append(response);
            initModal('response-dialog');
        }
    })
}

function initModal(divId){
    $("#"+divId).on("hidden.bs.modal", function(){
        $("#"+divId).remove(); //reset elements after loading
        window.location.reload();
    });

    $("#"+divId).modal("show");
}
